+++
date = "2015-10-07T19:56:34-05:00"
draft = false
title = "System Administration of WebLogic and Fusion Middleware"
Img = "6148871513_6b045c43ab_z.jpg"
Category = "Skills"
+++

I've been working with Oracle's Fusion Middleware Suite for about two years now.
In that time, I've learned quite a bit about the following:

* Weblogic Server 11g (10.3.6.*)
    * NodeManager
    * WebLogic Scripting Tool (WLST)
* Service Bus 11g (OSB)
* SOA Suite 11g (SOA)
* Business Transaction Management 12c (BTM)
* Oracle Enterprise Manager Cloud Control 12c (OEMCC)



If you have a question about the toolset, developing in it, maintaining it, or
otherwise trying to get something going with that, feel free to reach out.

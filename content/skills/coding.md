+++
date = "2015-10-12T18:08:27-05:00"
draft = false
title = "Coding / Languages"
Img = "github-logo.png"
Category = "Skills"
+++

Unfortunately, there's a lot of code I cannot share due to restrictions from
previous employers and whatnot, but I do still code on the side any projects
that I find would probably be better served in the public space.  If you look at
my current [Github Repo](https://github.com/klauern?tab=repositories), I have
a number of languages I've written code for.  I'm no expert, but languages don't
really freak me out the way they used to.  I've written code in at least the
following languages:

* [Clojure](https://github.com/search?utf8=%E2%9C%93&q=user%3Aklauern+language%3AClojure&type=Repositories&ref=searchresults)
* [Golang](https://github.com/search?utf8=%E2%9C%93&q=user%3Aklauern+language%3AGo&type=Repositories&ref=advsearch&l=Go&l=)
* [Ruby](https://github.com/search?utf8=%E2%9C%93&q=user%3Aklauern+language%3ARuby&type=Repositories&ref=searchresults)
* [Java 6 / 7 ](https://github.com/search?utf8=%E2%9C%93&q=user%3Aklauern+language%3ARuby&type=Repositories&ref=searchresults)(haven't dabbled in Java 8's newest stuff...yet)

Anyways, life is short, so this is merely what I have free time to contribute to
when I'm not otherwise occupied saving the world (Army), my family (4 kids under
10), or working.


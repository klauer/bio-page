+++
date = "2015-10-12T17:57:19-05:00"
draft = false
title = "About Me"
+++

I am a software developer and System Administrator of an Oracle Fusion Middleware
product suite at a regional utility company.  Having a background in Software
Engineering from the University of Wisconsin -- Platteville, I have always had a
keen eye on performance, brevity, and simplicity in design.

A second, more modest career is in being an active member of the United States
Army - Reserve component, where I drill regularly.  I don't do it for technical
skills, money, or prestige.  It provides me an immense set of intangibles in
terms of comraderie, teamwork, and the general feeling you get doing something
for a cause greater than your own.

At home, I am a father and husband, raising four young boys in between all of
my committments above.  My wife, Katrina is incredibly supportive and talented
in her own right.  I can't claim any of my achievements without recognizing the
incredible effort and support that she has provided me.
